let menuIcon = document.querySelector(".navbar__menu"),
    menu = document.querySelector(".menu-burger");

menuIcon.onclick = () => {
    menu.classList.toggle('active');
}
