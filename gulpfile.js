let project = "dist",
    source = "src";

let path = {
    build: {
        folder: project,
        html: project + "/",
        css: project + "/css/",
        js: project + "/js/",
        img: project + "/img/",
        fix: project + "/css/main.min.css"
    },
    dev:{
        html: source + "/*.html",
        scss: source + "/scss/*.scss",
        js: source + "/js/**/*.js",
        img: source + "/img/**/*.{jpg,png,svg}"
    }
}

let { dest } = require('gulp'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    browsersync = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del'),
    imagemin = require('gulp-imagemin')
    sass.compiler = require('node-sass');

function watcher() {
    browsersync.init({
        server: {
            baseDir: "./"
        },
        port: 3000
    });
    gulp.watch(path.dev.html).on('change', browsersync.reload)
    gulp.watch(path.dev.scss, buildSass).on('change', browsersync.reload)
    gulp.watch(path.dev.js, buildScript).on('change', browsersync.reload)
}

function buildSass() {
    return gulp.src('./' + source + '/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({cascade:false}))
    .pipe(gulp.dest(project + '/css'));
}

function buildScript(){
    return gulp.src('./' + source + '/js/*.js')
    .pipe(gulp.dest(project + '/js'));
}

function buildImage(){
    return gulp.src(path.dev.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.build.img))
}

function cleanDist() { 
    return del(['dist/**', '!dist']);
 }


gulp.task('dev', gulp.series(
    watcher,
    buildSass,
    buildScript,
    buildImage
))

gulp.task('build', gulp.series(
    cleanDist,
    buildSass,
    buildScript,
    buildImage
   
))