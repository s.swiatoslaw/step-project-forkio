const gulp = require('gulp'),
    clean = require('gulp-clean'),
    sass =require('gulp-sass'),
browserSync = require('browser-sync').create(),
concat=require('gulp-concat'),
imagemin=require('gulp-imagemin'),
autoprefixer = require('gulp-autoprefixer'),
cleanCSS = require('gulp-clean-css');

const paths = {
    src:{img:'./src/img/*.png',
        fix:'./dist/css/styles.min.css',
        styles:'./src/scss/**/*.scss',
        js:'./src/js/*.js',
        minCss:'./dist/css/*.css'
    },
    dist:{
        self:'./dist',
        styles: './dist/css',
        js:'./dist/js/',
        minImg:'./dist/img/'
    }
}
const cleanDist=()=>(
    gulp.src(paths.dist.self,{allowEmpty:true})
        .pipe(clean())
);
const  scssBuild=()=>(
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.dist.styles))
);
const buildJs=()=>(
    gulp.src(paths.src.js)
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.dist.js))
        .pipe(browserSync.stream())

)
const autofix=()=>(
    gulp.src(paths.src.fix)
        .pipe(autoprefixer({
            cascade:false
        }))
        .pipe(gulp.dest(paths.dist.styles))
)
const minImg=()=>(
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.minImg))
)
const  watcher=()=>{
    browserSync.init({
        server:{
baseDir:"./"
        }
    })
    gulp.watch(paths.src.styles,scssBuild).on('change',browserSync.reload)
gulp.watch(paths.src.js,buildJs).on('change',browserSync.reload)
}
gulp.task('build',gulp.series(
    cleanDist,
    scssBuild,
    autofix,
    buildJs,
    minImg,


))
gulp.task('dev',watcher);
gulp.task('minify-css',()=>{
    return gulp.src(paths.src.minCss)
        .pipe(cleanCSS({compatibility:'ie8'}))
            .pipe(gulp.dest(paths.dist.styles))
})
